﻿using System;
using System.Windows;

namespace SpbArchivesDownloader
{
    public class EntryPoint
    {
        [STAThread]
        public static void Main()
        {
            var app = new Application();
            MainWindow window = new MainWindow();
            app.Run(window);
        }
    }
}
