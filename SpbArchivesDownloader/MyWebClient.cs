﻿using HtmlAgilityPack;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace SpbArchivesDownloader
{
    public class MyWebClient
    {
        public CookieContainer Cookies { get; private set; }

        public MyWebClient()
        {
            Cookies = new CookieContainer();
        }

        public void ClearCookies()
        {
            Cookies = new CookieContainer();
        }
        public async Task<HtmlDocument> GetPage(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = WebRequestMethods.Http.Get;
            request.CookieContainer = Cookies;

            HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
            var stream = response.GetResponseStream();

            using (var reader = new StreamReader(stream))
            {
                string html = reader.ReadToEnd();
                var doc = new HtmlDocument();
                doc.LoadHtml(html);
                return doc;
            }
        }

        public async Task DownloadFile(string link, string savePath)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(link);
            request.Method = WebRequestMethods.Http.Get;
            request.CookieContainer = Cookies;
            HttpWebResponse httpResponse = (HttpWebResponse)await request.GetResponseAsync();
            Stream httpResponseStream = httpResponse.GetResponseStream();

            using (var fs = File.Create(savePath))
            {
                httpResponseStream.CopyTo(fs);
            }
        }
    }
}
