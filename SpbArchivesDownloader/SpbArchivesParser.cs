﻿using HtmlAgilityPack;
using SpbArchivesDownloader.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SpbArchivesDownloader
{
    internal class SpbArchivesParser
    {
        const string BASE_URL = "https://spbarchives.ru/infres";

        private readonly Random _random = new Random();

        private MyWebClient _webClient;

        public event EventHandler<string> StatusChanged = delegate { };
        public event EventHandler<Inventory> InventoryParsed = delegate { };
        public event EventHandler<Case> CaseParsed = delegate { };

        public async Task<IEnumerable<Inventory>> AnayseSpbarchivesFiles(string fondNumber)
        {
            StatusChanged(this, "Анализ фонда начат");

            _webClient = new MyWebClient();

            EventHandler<Inventory> setInventoryParsedStatus = (sender, inventory)
                => StatusChanged(this, $"Анализируется опись {inventory.Title}: \"{inventory.Name}\" [{inventory.CaseQuantity} дел]");
            EventHandler<Case> setCaseParsedStatus = (sender, oneCase)
                => StatusChanged(this, $"Анализируется опись {oneCase.Inventory.Title}: \"{oneCase.Inventory.Name}\"," +
                    $" дело {oneCase.Title}: \"{oneCase.Name}\" [{oneCase.ListQuantity} листов / {oneCase.ImageQuantity} изображений]");

            InventoryParsed += setInventoryParsedStatus;
            CaseParsed += setCaseParsedStatus;

            IEnumerable<Inventory> inventories = await GetInventories(fondNumber);

            foreach (Inventory inventory in inventories)
                inventory.Cases = await GetCases(inventory);

            InventoryParsed -= setInventoryParsedStatus;
            CaseParsed -= setCaseParsedStatus;

            StatusChanged(this, "Анализ фонда закончен");
            return inventories;
        }
        public async Task LoadSpbarchivesFiles(IEnumerable<Case> cases, string jsessionid, string savePath, string fondNumber)
        {
            if (!Directory.Exists(savePath))
            {
                StatusChanged(this, $"Папка {savePath} не существует");
                return;
            }

            string fondPath = Path.Combine(savePath, $"Фонд {fondNumber}");
            CreateIfNotExists(fondPath);

            _webClient = new MyWebClient();
            _webClient.Cookies.Add(new Cookie("JSESSIONID", jsessionid, "/", "spbarchives.ru"));

            int? allImageQuantity = cases.Sum(oneCase => oneCase.ImageQuantity);

            int index = 0;
            foreach (Case oneCase in cases)
            {
                string casePath = GetCasePath(fondPath, oneCase);
                CreateIfNotExists(casePath);

                string imagesPageLink = await GetImagesPageLink(oneCase);

                if (string.IsNullOrWhiteSpace(imagesPageLink))
                {
                    StatusChanged(this, $"Не удаётся получить ссылку на просмотрщик изображений. Возможно неверный JSESSIONID или не оплачен доступ");
                    return;
                }
                else
                {
                    IEnumerable<string> imageUrls = await GetImageUrls(imagesPageLink);

                    if (!imageUrls.Any())
                        StatusChanged(this, $"Не удаётся получить массив ссылок на изображения дела {oneCase.Title}: {oneCase.Name}");
                    else
                    {
                        foreach (var imageUrl in imageUrls)
                        {
                            string param = "_archiveStorePortlet_entityImageId=";
                            int start = imageUrl.IndexOf(param);
                            int end = imageUrl.IndexOf("&", start);
                            string name = imageUrl.Substring(start + param.Length, end - param.Length - start);
                            string imagePath = Path.Combine(casePath, $"{name}.jpg");
                            await _webClient.DownloadFile(imageUrl, imagePath);

                            await WaitRandomDelay();

                            index++;
                            StatusChanged(this, $"{index}/{allImageQuantity}: Скачано изображение {imagePath}");
                        }

                        await WaitRandomDelay();

                        StatusChanged(this, $"{index}/{allImageQuantity}:Скачаны все {oneCase.ImageQuantity} изображений дела {oneCase.Title}: {oneCase.Name}");
                    }
                }
            }

            StatusChanged(this, $"{index}/{allImageQuantity}: Скачаны все {cases.Count()} дел");
        }

        private async Task<IEnumerable<Inventory>> GetInventories(string fondNumber)
        {
            HtmlDocument document;

            var inventories = new List<Inventory>();
            int pageNumber = 1;
            while(true)
            {
                document = await _webClient.GetPage($"{BASE_URL}/-/archive/cgia/{fondNumber}?_archiveStorePortlet_delta=100&_archiveStorePortlet_cur={pageNumber}");

                HtmlNodeCollection rowNodes = document
                    .DocumentNode
                    .SelectNodes("//*[@id=\"_archiveStorePortlet_invDocsSearchContainerSearchContainer\"]/table/tbody/tr");

                if (document == null)
                    break;

                if (rowNodes == null)
                    break;

                foreach(var rowNode in rowNodes)
                {
                    var digitizedNode = rowNode.SelectSingleNode("./td[2]/div/div/span[contains(text(),'Есть оцифрованные дела')]");
                    bool isDigitized = digitizedNode != null;
                    if (isDigitized)
                    {
                        string titleText = rowNode.SelectSingleNode("./td[1]/div/div/h2/text()")?.InnerText;
                        int? titleNumber = string.IsNullOrWhiteSpace(titleText)
                            ? null
                            : Convert.ToInt32(new string(titleText.Where(x => char.IsNumber(x)).ToArray()));
                        string link = rowNode.SelectSingleNode("./td[2]/div/div/a")?.GetAttributeValue("href", string.Empty);
                        string name = rowNode.SelectSingleNode("./td[2]/div/div[1]/a")?.InnerText.Trim();
                        string quantityText = rowNode.SelectSingleNode("./td[3]/div/div")?.InnerText;
                        int? quantity = string.IsNullOrWhiteSpace(quantityText)
                            ? null
                            : Convert.ToInt32(new string(quantityText.Where(x => char.IsNumber(x)).ToArray()));

                        if (!string.IsNullOrWhiteSpace(link))
                        {
                            var inventory = new Inventory
                            {
                                Title = titleText,
                                TitleNumber = titleNumber,
                                Link = link,
                                Name = name,
                                CaseQuantity = quantity
                            };
                            InventoryParsed(this, inventory);
                            inventories.Add(inventory);
                        }
                    }
                }
                pageNumber++;
            }
            return inventories;
        }
        private async Task<IEnumerable<Case>> GetCases(Inventory inventory)
        {
            HtmlDocument document;

            var cases = new List<Case>();
            int pageNumber = 1;
            while (true)
            {
                document = await _webClient.GetPage($"{inventory.Link}?_archiveStorePortlet_delta=100&_archiveStorePortlet_cur={pageNumber}");

                if (document == null)
                    break;

                var rowNodes = document
                    .DocumentNode
                    .SelectNodes("//*[@id=\"_archiveStorePortlet_unitDocsSearchContainerSearchContainer\"]/table/tbody/tr");

                if (rowNodes == null)
                    break;

                foreach (var rowNode in rowNodes)
                {
                    var digitizedNode = rowNode.SelectSingleNode("./td[2]/div/div/span[contains(text(),'Оцифровано')]");
                    bool isDigitized = digitizedNode != null;
                    if (isDigitized)
                    {
                        string titleText = rowNode.SelectSingleNode("./td[1]/h2/text()")?.InnerText;
                        int? titleNumber = ParseNumber(titleText);
                        string link = rowNode.SelectSingleNode("./td[2]/div/div/a")?.GetAttributeValue("href", string.Empty);
                        string name = rowNode.SelectSingleNode("./td[2]/div/div[1]/a")?.InnerText.Trim();
                        string imageQuantityText = rowNode.SelectSingleNode("./td[2]/div/div/span[contains(text(),'Оцифровано')]")?.InnerText;
                        int? imageQuantity = ParseNumber(imageQuantityText);
                        string listQuantityText = rowNode.SelectSingleNode("./td[3]/div/text")?.InnerText;
                        int? listQuantity = ParseNumber(listQuantityText);

                        if (!string.IsNullOrWhiteSpace(link))
                        {
                            var newCase = new Case
                            {
                                Title = titleText,
                                TitleNumber = titleNumber,
                                Link = link,
                                Name = name,
                                ImageQuantity = imageQuantity,
                                ListQuantity = listQuantity,
                                Inventory = inventory
                            };
                            CaseParsed(this, newCase);
                            cases.Add(newCase);
                        }
                    }
                }
                pageNumber++;
            }
            return cases.Distinct().ToArray();
        }
        private async Task<string> GetImagesPageLink(Case oneCase)
        {
            HtmlDocument document;

            document = await _webClient.GetPage(oneCase.Link);

            if (document == null)
                return null;

            string imagesPageLink = document
                .DocumentNode
                .SelectSingleNode("//*[@id=\"portlet_archiveStorePortlet\"]/div/div/div/div/div[2]/div[7]/a")
                ?.GetAttributeValue("href", string.Empty);

            return imagesPageLink;
        }
        private async Task<IEnumerable<string>> GetImageUrls(string imagesPageLink)
        {
            HtmlDocument document;
            var imageUrls = new List<string>();

            document = await _webClient.GetPage(imagesPageLink);

            if (document == null)
                return Enumerable.Empty<string>();

            string script = document
                .DocumentNode
                .Descendants()
                .FirstOrDefault(node => node.Name == "script" && node.InnerText.Contains("var images"))
                ?.InnerText;

            if (script == null)
                return Enumerable.Empty<string>();

            var startIndex = script.IndexOf("var images");
            var endIndex = script.IndexOf(";", startIndex);

            string cutScript = script.Substring(0, endIndex + 1);
            string scriptWithReturn = "(function() { " + cutScript + " return images; })()";

            Jurassic.ScriptEngine engine = new Jurassic.ScriptEngine();
            dynamic result = engine.Evaluate(scriptWithReturn);
            uint length = result.Length;
            for (uint i = 0; i< length; i++)
            {
                var imageUrl = result[i]["imageUrl"];
                imageUrls.Add(imageUrl);
            }

            return imageUrls;
        }
        private string GetCasePath(string savePath, Case oneCase)
        {
            string invName = RemoveInvalidChars($"{oneCase.Inventory.TitleNumber} - {oneCase.Inventory.Name.Trim()}");
            string caseName = RemoveInvalidChars($"{oneCase.TitleNumber} - {oneCase.Name.Trim()}");
            string casePath = Path.Combine(savePath, invName, caseName);
            if (casePath.Length > 200)
                casePath = casePath.Substring(0, 200).Trim();
            return casePath;
        }
        private string RemoveInvalidChars(string filename)
            => string.Concat(filename.Split(Path.GetInvalidFileNameChars()));
        private static int? ParseNumber(string titleText)
        {
            if (string.IsNullOrWhiteSpace(titleText))
                return null;

            var onlyNumberString = new string(titleText.Where(x => char.IsNumber(x)).ToArray());

            if (string.IsNullOrWhiteSpace(onlyNumberString))
                return null;

            var number = Convert.ToInt32(onlyNumberString);
            return number;
        }
        private async Task WaitRandomDelay()
        {
            int delay = (int)(1000 * (1 + 2 * _random.NextDouble()));
            await Task.Delay(delay);
        }
        private void CreateIfNotExists(string casePath)
        {
            if (!Directory.Exists(casePath))
                Directory.CreateDirectory(casePath);
        }
    }
}
