﻿namespace SpbArchivesDownloader.Entities
{
    internal class Case
    {
        public Inventory Inventory { get; set; }
        public int? TitleNumber { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public int? ListQuantity { get; set; }
        public int? ImageQuantity { get; set; }
        public string Link { get; set; }
    }
}
