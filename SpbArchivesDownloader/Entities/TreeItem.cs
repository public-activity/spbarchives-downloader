﻿using System.Collections.Generic;
using System.ComponentModel;

namespace SpbArchivesDownloader.Entities
{
    public class TreeItem : ITreeItem
    {
        TreeItem _parent;

        public object Entity { get; }
        public TreeItem(object entity, string name, bool isExpanded, bool isInitiallySelected = false)
        {
            Entity = entity;
            Name = name;
            IsExpanded = isExpanded;
            IsInitiallySelected = isInitiallySelected;

            Children = new List<ITreeItem>();
        }

        public void Initialize()
        {
            foreach (TreeItem child in Children)
            {
                child._parent = this;
                child.Initialize();
            }
        }

        public List<ITreeItem> Children { get; set; }

        public bool IsInitiallySelected { get; }

        public string Name { get; private set; }

        bool? _isChecked = false;
        public bool? IsChecked
        {
            get { return _isChecked; }
            set { SetIsChecked(value, true, true); }
        }

        public bool IsExpanded { get; private set; }

        public void SetIsChecked(bool? value, bool updateChildren, bool updateParent)
        {
            if (value == _isChecked)
                return;

            _isChecked = value;

            if (updateChildren && _isChecked.HasValue)
                Children.ForEach(c => c.SetIsChecked(_isChecked, true, false));

            if (updateParent && _parent != null)
                _parent.VerifyCheckState();

            OnPropertyChanged("IsChecked");
        }

        void VerifyCheckState()
        {
            bool? state = null;
            for (int i = 0; i < Children.Count; ++i)
            {
                bool? current = Children[i].IsChecked;
                if (i == 0)
                {
                    state = current;
                }
                else if (state != current)
                {
                    state = null;
                    break;
                }
            }
            SetIsChecked(state, false, true);
        }

        void OnPropertyChanged(string prop)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
