﻿using System.Collections.Generic;
using System.ComponentModel;

namespace SpbArchivesDownloader.Entities
{
    public interface ITreeItem : INotifyPropertyChanged
    {
        public object Entity { get; }
        List<ITreeItem> Children { get; }
        bool? IsChecked { get; set; }
        bool IsExpanded { get; }
        void SetIsChecked(bool? value, bool updateChildren, bool updateParent);
        bool IsInitiallySelected { get; }
        string Name { get; }
    }
}
