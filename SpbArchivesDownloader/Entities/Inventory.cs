﻿using System.Collections.Generic;

namespace SpbArchivesDownloader.Entities
{
    internal class Inventory
    {
        public int? TitleNumber { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public int? CaseQuantity { get; set; }
        public string Link { get; set; }
        public IEnumerable<Case> Cases { get; set; }
    }
}
