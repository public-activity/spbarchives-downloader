﻿using SpbArchivesDownloader.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Forms;

namespace SpbArchivesDownloader
{
    public partial class MainWindow : Window
    {
        private readonly ObservableCollection<ITreeItem> _treeItems;
        private readonly SpbArchivesParser _parser;

        public MainWindow()
        {
            InitializeComponent();

            _treeItems = new ObservableCollection<ITreeItem>();
            _tree.ItemsSource = _treeItems;

            _parser = new SpbArchivesParser();
            _parser.StatusChanged += (sender, args) => _status.Text = args;

            var assembly = Assembly.GetExecutingAssembly();
            var assemblyName = assembly.GetName();
            _version.Text = $"{assemblyName.Version.Major}.{assemblyName.Version.Minor}.{assemblyName.Version.Build}";
        }

        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new FolderBrowserDialog())
            {
                dialog.ShowNewFolderButton = true;
                DialogResult result = dialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                    _savePath.Text = dialog.SelectedPath;
            }
        }
        private async void Start_Analyse_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(_fondNumber.Text))
                _status.Text = "Укажите номер фонда";
            else
            {
                _treeItems.Clear();

                IEnumerable<Inventory> inventories = await _parser.AnayseSpbarchivesFiles(_fondNumber.Text);

                if (inventories.Any())
                {
                    ITreeItem fondTree = ConvertFondToTree(_fondNumber.Text, inventories);
                    _treeItems.Add(fondTree);
                }
                else
                {
                    _status.Text = "Описи с изображениями не обнаружены";
                }
            }
        }
        private async void Start_Download_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(_jsessionid.Text) || string.IsNullOrWhiteSpace(_savePath.Text))
                _status.Text = "Укажите JSESSIONID и путь для скачивания";
            else
            {
                IEnumerable<Case> selectedCases = _treeItems
                    .SelectMany(fond => fond.Children)
                    .SelectMany(inventory => inventory.Children)
                    .SelectMany(caseStack => caseStack.Children)
                    .Where(oneCase => oneCase.IsChecked == true)
                    .Select(oneCase => oneCase.Entity)
                    .Cast<Case>()
                    .ToArray();

                if (selectedCases.Any())
                    await _parser.LoadSpbarchivesFiles(selectedCases, _jsessionid.Text, _savePath.Text, _fondNumber.Text);
                else
                    _status.Text = "Не выбраны дела для скачивания";
            }
        }
        private ITreeItem ConvertFondToTree(string fondNumber, IEnumerable<Inventory> inventories)
        {
            TreeItem root = new TreeItem(null, fondNumber, true)
            {
                Children = inventories.Select(inv => ConvertInventoryToTreeItem(inv)).ToList()
            };
            root.Initialize();
            return root;
        }
        private ITreeItem ConvertInventoryToTreeItem(Inventory inventory)
        {
            IEnumerable<int?> titles = inventory.Cases.Select(oneCase => oneCase.TitleNumber);
            IEnumerable<int?> stacks = titles.Select(title => (title - 1) / 100)
                .Distinct()
                .OrderBy(stack => stack.Value);

            List<ITreeItem> caseStacks = new List<ITreeItem>();
            foreach (int? stack in stacks)
            {
                IEnumerable<Case> stackCases = inventory.Cases.Where(oneCase => (oneCase.TitleNumber - 1) / 100 == stack);
                string stackName = $"{stack * 100 + 1}-{stack * 100 + 100} [{stackCases.Count()} дел]";
                TreeItem stackTree = new TreeItem(null, stackName, false)
                {
                    Children = stackCases.Select(oneCase => ConvertCaseToTreeItem(oneCase)).ToList()
                };
                caseStacks.Add(stackTree);
            }

            string inventoryName = $"{inventory.Title} ({inventory.Name}) [{inventory.CaseQuantity} дел]";
            TreeItem inventoryTree = new TreeItem(inventory, inventoryName, true)
            {
                Children = caseStacks
            };

            return inventoryTree;
        }
        private ITreeItem ConvertCaseToTreeItem(Case oneCase)
        {
            string name = $"{oneCase.Title} ({oneCase.Name}) [{oneCase.ListQuantity} листов / {oneCase.ImageQuantity} изображений]";
            TreeItem caseTree = new TreeItem(oneCase, name, false);
            return caseTree;
        }
    }
}
